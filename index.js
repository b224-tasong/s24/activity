// alert("Hi!");

let num = parseInt(prompt("Enter Number: "));
let getCube = num ** 3;
console.log(`The cube of ${num} is ${getCube}`);

const address = ["Del Pilar St", "Brgy. Silongan", "Butuan City"];

const [addressInfo1, addressInfo2, addressInfo3] = address;

console.log(`I live at ${addressInfo1} ${addressInfo2} ${addressInfo3}`);


const animal = {
	name: "Lolong",
	type: "saltwater",
	weight: 1075,
	measurement: "20 ft 3",
}

function getAnimalInfo({name, type, weight, measurement}) {
	console.log(`${name} was a ${type}. He weighed ${weight} kgs with a measurement of ${measurement} in`);
};

getAnimalInfo(animal);

let numArray = [1, 2, 3, 4, 5];

numArray.forEach((num)=>{
	console.log(`${num}`);
}) ;


class Dog{
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed =  breed;
	};
};

const mydog = new Dog("Lebron", 40, "Pitbul");

console.log(mydog);